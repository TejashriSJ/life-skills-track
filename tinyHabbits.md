# Tiny Habits

### Question 1

##### Your takeaways from the video (Minimum 5 points)

- To achieve any big changes in our daily habits, should start practicing them with a tiny habit rather than pressurizing ourselves for making big changes.
- Practice tiny habits by doing a new tiny behavior after an existing habit.
- Placing a tiny habit on the right path that is practicing it after an existing habit makes it grow easily.
- Repeatedly making a smaller improvement over a long period make it a habit unknowingly.
- Should have the motivation and a trigger for developing a habit.

### Question 2

##### Your takeaways from the video in as much detail as possible

- Shrink the behavior: Shrinking every habit to the tiniest possible version so that very low motivation is needed to do it.
- Identify an action prompt: Use the Action prompts over the external and internal prompts.
- Grow your habit with some shine: Learn to celebrate after a tiny win so that your confidence and motivation increase to do that habit again.
- The effort of learning to celebrate is a small price to pay for becoming an expert in that.

### Question 3

##### How can you use B = MAP to make making new habits easier?

- Shrink a new habit to the tiniest version of it.
- Use an action promptly to trigger your tiny habit.
- Always celebrate the completion of your tiny habit.

### Question 4

##### Why it is important to "Shine" or Celebrate after each successful completion of a habit?

It is very important to shine or celebrate after each successful completion of a habit because celebrating success increases our confidence and increases our motivation to do that again.

### Question 5

##### Your takeaways from the video (Minimum 5 points)

- According to James Clear we can achieve or build habits by following four stages that are, noticing, wanting, doing, and liking.
- Make a proper plan and have proper clarity of what we are doing and what may happen if we don't do this for achieving any behavior.
- Focus on the process, not the outcome, focus on daily actions, and celebrate every small win.
- Creating barriers for our bad habits like removing the social media apps at our quick search so that habit of opening apps once we use our phone will be reduced.
- Be consistent in our habits if it's not possible to follow for a day then should not discourage or quit instead continue with the practice.
- Practicing small actions over a long period makes us achieve what we want.

### Question 6

###### Write about the book's perspective on habit formation from the lens of Identity, processes, and outcomes?

Identity is about what we believe, processes are about what we do and outcomes are about what we get. With identity-based habits, we will concentrate more on who we wish to become. Our habits should become part of our identity.

When we solve a problem in terms of outcomes and results, we solve that problem temporarily, but to solve the problem in the longer term we should change our identity.

### Question 7

###### Write about the book's perspective on how to make a good habit easier?

Making good habits easier includes 4 stages,

**Cue**: Make it obvious, create an environment like you want to put fewer steps between you and the good behaviors.Initiate the action, and make the first move.

**Craving**: Make it attractive, Making our habit attractive make us stick to it and make it effortless.

**Response**: Make it easy, Make the habit easier by creating fewer steps.

**Reward**: Make it immediately satisfying, because what is immediately rewarded is repeated.

### Question 8

##### Write about the book's perspective on making a bad habit more difficult?

- Creating an environment such that you want to put more steps between you and the bad behaviors.
- Make it immediately unsatisfying, because what is immediately punished is avoided.

## 5. Reflection:

### Question 9:

##### Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

I will learn a new word every day so that I can improve my vocabulary.

- I will install an application that helps me in learning a new word.
- I will keep that application on my home page.
- I will learn every day after having my dinner.
- I feel happy about what I learned in a day.
- I will continue this process without any breaks.

### Question 10:

##### Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?

I will avoid using social media more.

- I will remove the apps from my home page.
- I feel bad for wasting time on social media.
