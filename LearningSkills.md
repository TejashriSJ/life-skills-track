# Learning Skills

### Question 1

##### What is the Feynman Technique? Paraphrase the video in your own words.

Feynman Technique includes mainly four steps.

1. Get paper and write the name of the topic which you are learning
2. Explain what you have learned in simple terms as you are teaching someone.
3. Go through the topics again that you could find stuck or find difficult to explain.
4. Go through your explanation and break down the words which you are finding as complex into simpler words.

### Question 2

What are the different ways to implement this technique in your learning process?

1. In our learning process, After reading any of the concepts we can try to write what and all we have understood simply like we are explaining to someone.
2. We can read the topics for which we get stuck while explaining.

### Question 3

Paraphrase the video in detail in your own words.

In the video they have explained how to read effectively, To read effectively we need to follow one major process which is to focus and relax. This means while reading we should not overload or we should not procrastinate things but we have to be 100% focused while we are reading after that we should relax our minds to learn effectively.

We should follow steps like keeping a timer for 25 minutes so that we will read for 25 minutes without
any distraction and with 100% focus later we have to take rest for about 10 minutes this will improve our learning efficiency.

And while reading by keeping a timer we should not think like we will complete this topic in this time, but we should think like I will learn with 100 % focus for these minutes.

And while reading we should try to recall the concepts which we had learned not by reading again or marking points, but we have to see the page and by not looking at it we should say in our mind what and all we have learned before moving into next topic.

### Question 4

What are some of the steps that you can take to improve your learning process?

1. Keeping timer for some minutes to read effectively without distraction. And taking rest after the particular time.
2. Recalling the concepts which were learned before moving to the next topic.
3. Giving 100 % attention to what we are doing, not involving two or more works at a time.

### Question 5

Your key takeaways from the video? Paraphrase your understanding.

1. We can learn anything if we dedicate 20 hrs of our time.
2. After deciding what to learn we have to collect important resources to learn.
3. Then we have to start learning and should not procrastinate.
4. And with 100% attention we have to learn.
5. After learning everything we have to practice it for at least for 20hrs.

### Question 6

What are some of the steps that you can while approaching a new topic?

1. I will collect the resources for learning a new topic and I will select the important one to start with it.
2. I will keep a timer for reading and I will learn with 100% attention and concentration.
3. Should avoid all types of distractions while reading.
4. Once I read everything I will practice much to make sure that I understood everything clearly.
