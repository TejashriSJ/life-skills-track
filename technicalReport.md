

# Messaging Queues


#### Contents
-----

1. [What are Messaging Queues?](#what-are-messaging-queues)
2. [Why they are used?](#why-they-are-used)
3. [What are popular tools?](#what-are-popular-tools) 
4. [What is Enterprise Message Bus?](#what-is-enterprise-message-bus)

-----

### What are Messaging Queues?

Messaging queues are generally a queing service used in serverless architectures and microservices to decouple different components. These components are easier to develop, scale, and maintain in the long term.

It is a form of asynchronous service-to-service communication used in serverless and microservices architectures. Here the messages will be stored in the queue until they are processed and deleted.
 
 Each one of the messages will be processed only once in a time, by a single consumer. And it can be used to decouple heavyweight processing, buffer or batch work, and smooth spiky workloads.

Usually, in many of the applications these are used and some producers create and put the messages into message queues. And there are consumers or work processes which read those messages and take action. Any of the applications which are written using this interface are known as message queuing applications where,
- Messaging means that programs communicate by sending each of the data in messages instead of calling each other directly.
- Queueing means that messages are placed on queues in storage, it allows programs to run independently of each other without having a logical connection between them.



### Why they are used?

Message queuing has a wide range of applications in data processing and it is most commonly used in electronic mail nowadays. To send an electronic message over a long distance requires queuing, otherwise it requires every node on the route to be available for forwarding messages, and also the addressees to be logged on.

In a queuing system, messages are stored at intermediate nodes until the system is ready to forward them. And at the end, they are stored in an electronic mailbox until the addressee is ready to read them.

We can see even now many complex business transactions are processed today without queuing. In a large network, the system might be maintaining many thousands of connections in a ready-to-use state. If any one part of the system gets into trouble means it will affect many parts of the system so it is very important to use message queuing 

In a message queuing environment, each program that is part of an application suite performs a self-contained, well-defined function in response to a specific request. To communicate with another program, a program must put a message on a predefined queue. The other program retrieves the message from the queue and processes the requests and information contained in the message. So message queuing is a style of program-to-program communication. 



### What are popular tools?

We can see many tools for message queuing, some most popular tools are mentioned below.
- Kafka
- RabbitMQ
- Amazon SQS
- Astra Streaming
- ActiveMQ

"High-throughput" is the primary reason developers pick Kafka over its competitors, while "It's fast and it works with good metrics/monitoring" is the reason why RabbitMQ was chosen. 

Here are the logos of Kafka, RabbitMq , Amazon SQS and Astra Streaming respectively. Click on the image for more information.

[![Kafka](https://img.stackshare.io/service/1063/kazUJooF_400x400.jpg)](https://www.trustradius.com/products/apache-kafka/reviews#product-details)
[![RabbitMQ](https://img.stackshare.io/service/1061/default_df93e9a30d27519161b39d8c1d5c223c1642d187.jpg)](https://www.trustradius.com/products/rabbitmq/reviews)
[![Amazon SQS](https://img.stackshare.io/service/395/amazon-sqs.png)](https://www.trustradius.com/products/amazon-simple-queue-service-sqs/reviews#overview)
[![Astra Streaming](https://media.trustradius.com/product-logos/YW/hI/37O0BHHJU9DI-180x180.JPEG)](https://www.trustradius.com/products/datastax-astra-streaming-datastax/reviews)



### What is Enterprise Message Bus?

Enterprise Message Bus is one of the software platform used to distribute work among connected components of an application. It is a type of software platform known as middleware, which works behind the scenes to provide application-to-application communication. It is designed to provide a uniform means of moving work, offering applications the ability to connect to this and subscribe to messages based on simple structural and business policy rules.

An Enterprise Message Bus implements a communication system between mutually interacting software applications in a service-oriented architecture (SOA). It represents a software architecture for distributed computing, and is a special variant of the more general client-server model, any application may behave as server or client. It promotes agility and flexibility with regard to high-level protocol communication between applications. Its primary use is in enterprise application integration (EAI) of heterogeneous and complex service landscapes. 

-----

#### References

- https://www.ibm.com/docs/en/ibm-mq/9.1?topic=overview-introduction-message-queuing
- https://stackshare.io/message-queue
- https://en.wikipedia.org/wiki/Enterprise_service_bus






