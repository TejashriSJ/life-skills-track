# Focus Management

### Question 1

##### What is deep work?

As explained in the video, Focusing on a cognitively demanding task without any distraction which increases the quality of work is called Deep Work.

### Question 2

##### Paraphrase all the ideas in the above videos and this one **in detail**.

- As explained in the video Optimal duration for deep work is at least 1 hour and we should not switch between a context for at least an hour during deep work. Duration of one hour of deep work will make us use 40 - 45 minutes fruitfully.

- Deadlines help us be motivated and help us by avoiding procrastinating things, If we keep deadlines for breaks it will help us in avoiding unnecessary breaks in the middle. So breaks and distractions must be scheduled. And also deadlines should be reasonable to a humane capacity because too much pressure can reduce productivity.

- In the book, Newport argues that deep work is becoming increasingly rare in our society due to the rise of technology and constant connectivity. He believes that the ability to focus deeply and work without distraction is becoming more valuable in our economy and that those who cultivate this skill are more likely to be successful in their careers.

- In the book, He argues that deep work is becoming increasingly rare in our society due to the rise of technologies like social media which make us stay in constant connectivity.

- He believes that the ability to focus deeply and work without distraction is becoming more valuable in our economy some of the persons who got succeeded in their work by doing deep work are J K Rowling, Bill Gates, and author Cal Newport.

- According to him, we have to practice deep work in a rhythmic way better to practice in the morning as the distractions are less.

- And we should mark boundaries for distractions and we should schedule our distractions so that we can work deeply.

### Question 3

##### How can you implement the principles in your day-to-day life?

- Working more in the morning.

- Scheduling break/Distraction link avoiding social media for a duration.

- Maintaining a rhythm of practicing deep work every day.

- Having proper sleep.

### Question 4

##### Your key takeaways from the video Dangers of Social Media

- Social media is not only a distraction, but it can also have a negative impact on our well-being and ability to focus.

- He suggests people quit social media or at least gradually reduce their usage, as we can be connected to people, informed about current events, and get entertained by many other platforms with no need for social media for this.

- We should follow our passion and pursue meaningful work instead of wasting time using social media.

- And he explains that social media creates a shallowness in our thinking and interactions.

- He suggests that quitting social media can lead to a more focused and meaningful life.
