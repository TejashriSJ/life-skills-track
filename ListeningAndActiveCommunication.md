# Listening and Active Communication

### 1. Active Listening

##### Question 1

###### What are the steps/strategies to do Active Listening?

1. Avoid getting destracted by our own thoughts.
1. Try not to interupt the other person, let them finish and then respond.
1. Use door openers.
1. Use body language.
1. Take notes of important conversations.
1. Have eye contact with the person who is speaking.

### 2. Reflective Listening

##### Question 2

###### According to Fisher's model, what are the key points of Reflective Listening?

The key points of Reflective Listening According to Fisher's model are,

1. We need to completly involve in the conversation by reducing the distractions of any type which will not allow us to pay attention to the conversation.

2. We should understand and think in the speaker's point of view. That means we are not agreeing with the speaker, we are just viewing situations in their perspective. We should encourage the person to speak freely by not being judgmental.

3. We should understand the mood of the speaker and we should reflect their state with our words, this will make the speaker feel better. And while reflecting we should be aware of our voice and the posture.

### 3. Reflection

##### Question 3

###### What are the obstacles in your listening process?

1. Sometimes I will be in my thought process and I won't listen to the person who is speaking to me.
1. And Sometimes I interupt the person for conveying my thoughts which are build over his/her topic.

##### Question 4

###### What can you do to improve your listening?

1. I will be attentive to my body language, eye contact, and strategies for Listening while talking to a person.
1. And I can improve listening by having the patience to wait till the person completes his/her thoughts.

### 4. Types of Communication

##### Question 5

###### When do you switch to Passive communication style in your day to day life?

In some situations like when I will be with a group of people, If I was getting disturbed by others when they are speaking loudly, I won't tell them to talk slowly instead I make myself tolerate that.

##### Question 6

###### When do you switch into Aggressive communication styles in your day to day life?

In some situations like when I was in frustration, I may hurt or discomfort someone who is speaking with me.

##### Question 7

###### When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

I usually do not switch into Passive Aggressive communication styles, In some situations like when my friend shows me her new dress if I feel not to praise her I will tell like "it's nice, even I want to buy this but I’m too skinny for this style" It may feel bad for her.

##### Question 8

###### How can you make your communication assertive?

1. We should learn to regognize and name other's feelings, we should not make judgemts about other's feelings.
2. We should learn to recognize and name what we need, we should start looking for the need behind the need.
3. In order to practice assertive communication we should choose safe person and easy situations.
4. We should be aware of our body language while speaking, because body language also tells us way of communication.
5. We should speak as early as possible about our thoughts otherwise it will be a problem for us.
