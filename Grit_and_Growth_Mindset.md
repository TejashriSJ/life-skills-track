# Grit and Growth Mindset

## 1. Grit

#### Question 1

###### Paraphrase (summarize) the video in a few lines. Use your own words.

In the video she found that IQ is not the only difference between the best and worst students, she got to know that anyone can learn if they work hard and long enough.

She has compared many students in many different contexts and came to the conclusion that the best performance is not based on their physical health, or social intelligence and also not IQ but the 'Grit'.

According to her, Grit is passion and perseverance for a very long-term goal.
It is having stamina, sticking with your future day in and day out not for a week not for months but for years. Grit is a marathon, not a sprint.
we should develop a growth mindset.

#### Question 2

###### What are your key takeaways from the video to take action on?

- Developing a growth mindset.
- Should stick to our goal/passion for years and we should work on it every day.

## 2. Introduction to Growth Mindset

#### Question 3

###### Paraphrase (summarize) the video in a few lines in your own words.

In the video, they mainly focused on improving the way we learn.
They compared the different people's mindsets and classified them as fixed mindsets and growth mindsets.
In the learning process, people's mindset plays an important role.

People with a fixed mindset think that skills and intelligence are fixed they come by birth we cannot change and it is not in control of our ability
they believe that skill is brown so don't have to learn, and as they mainly focus on performance output they don't believe their effort will give them a good result.

But people with a growth mindset think that skills and intelligence are grown and developed and we are in control of our abilities.
they believe that skills are built so they learn to develop them, and as they focus more on the process of learning they less care about the result.

#### Question 4

###### What are your key takeaways from the video to take action on?

- Should have a growth mindset.
- Should focus more on the process of learning rather than the result.
- Should accept the feedback or suggestions from others and try to improve it.
- Do not be afraid of doing mistakes as mistakes make you learn better.

## 3. Understanding Internal Locus of Control

#### Question 5

###### What is the Internal Locus of Control? What is the key point in the video?

Internal Locus of Control is believing that we are responsible for our work and believing in our ability to do the work.

- We should have an internal locus of control.
- We should believe in our efforts in doing the work rather than believing in our external entities like luck, smartness, etc which are not in our ability to control.
- We can adapt internal locus of control simply by solving the problem in our life and taking some time to appreciate that our actions solved the problem.

## 4. How to build a Growth Mindset

#### Question 6

###### Paraphrase (summarize) the video in a few lines in your own words.

The main thing and the first thing in developing a growth mindset is believing in your abilities to figure things out. We should not think that we cannot solve this, I don't have that much knowledge but have to think that how can I solve this, for solving what and all I should learn.

And we have to question your negative assumptions,
like why can't I do this, why it is not possible for me etc.

And the third thing is when we get discouragement in not solving or not completing a task we should take it positively, we should not quit but we should honor the struggle.

#### Question 7

###### What are your key takeaways from the video to take action on?

- I believe in my abilities to do the work.
- I will not blame anything or anyone for my failure in doing anything.
- And any time I get a negative thought about my capabilities I will question myself why can't.
- I will always honor the struggle I faced during the task rather than getting discouraged.

## 4. Mindset - A MountBlue Warrior Reference Manual

#### Question 8

###### What are one or more points that you want to take action on from the manual? (Maximum 3)

1. I will treat new concepts and situations as learning opportunities. I will never get pressurized by any concept or situation.
1. I will understand each concept properly.
1. I will not leave my code unfinished till I complete the following checklist:
   - Make it work.
   - Make it readable.
   - Make it modular.
   - Make it efficient.
