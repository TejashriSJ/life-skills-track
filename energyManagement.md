# Energy Management

## 1. Manage Energy Not Time

### Question 1:

##### What are the activities you do that make you relax - Calm quadrant?

- Playing games
- Eating Ice Creams
- Listening to music

### Question 2:

##### When do you find getting into the Stress quadrant?

- When learning new things and not understanding them after reading for a long time.
- When I have many things to submit in less time.
- When I am stuck in a problem for a long time.

### Question 3:

##### How do you understand if you are in the Excitement quadrant?

- I feel like doing more or learning more when I am in the Excitement quadrant.

### Question 4

##### Paraphrase the Sleep is your Superpower video in detail.

In the video, he explained the importance of sleep. For us sleeping after reading is one way of a good thing that it will recollect everything and helps us to remember, in the same way sleeping before reading will make our brain get ready for storing things, sleeping makes our brain more absorbable towards what we read.

Lack of sleep may lead to many problems like aging, heart attacks, immune deficiency, worse memory, etc and their survey on people tells that people who met with an accident and who committed suicide were having lack of sleep.

We have to sleep for the whole 8hr in the night for a better living, to get better sleep we have to maintain a regular sleep like sleeping every day for a particular time and we have to keep the room cool to get better sleep.

### Question 5

What are some ideas that you can implement to sleep better?

- I will try to follow sleeping every day at the same time for 8hrs.
- I will keep my room cool when I will sleep.

## 5. Brain Changing Benefits of Exercise

### Question 6

##### Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points.

The video tells us the positive impacts of exercise on the brain. And she explained how exercise can increase the production of chemicals such as dopamine, which can improve our mood and reduce stress.

Exercise can lead to the growth of new brain cells in the hippocampus by which our learning and memory capacity will increase.

Physical activities will give us a better mood, better energy, better memory, and better attention.

Exercises have an immediate effect on the brain, we can feel the changes instantly.

It improves our ability to shift and focus attention.

### Question 7

##### What are some steps you can take to exercise more?

- I will try to take steps instead of taking a lift every day.
- I will walk fast when I go to my pg.
- I will try to do yoga at least for 10 minutes a day.
