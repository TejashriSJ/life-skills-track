# Good Practices for Software Development

## Review Questions

### Question 1

###### What is your one major takeaway from each one of the 6 sections. So 6 points in total.

- Make sure to ask questions and seek clarity in the meeting itself.

- If the implementation taking longer than usual due to some unexpected issue, Inform relevant team members.

- Explain the problem clearly, mention the solutions I tried out to fix the problem.

- Join the meetings 5-10 mins early to get some time with our team members. This will helps in improving team bond.

- Be available when someone replies to your message. Things move faster if it's a real-time conversation as compared to a back and forth every hour.

- Work when you work, play when you play is a good thumb rule. Doing things with 100% attention is a major thing.

### Question 2

##### Which area do you think you need to improve on? What are your ideas to make progress in that area?

I need to improve on knowing our team members.

- I will try to join the meating early so that I can initiate speaking with my team members.

- I will make the time for my company and the product I am working on and also for my team members.
