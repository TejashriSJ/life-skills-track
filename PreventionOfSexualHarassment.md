# Prevention of Sexual Harassment

## Question 1:

#### What kinds of behaviour cause sexual harassment?

Sexual harassment is any undesirable action based on the sexual nature that a person takes which affects the work environment and makes the work environment hostile.

Three kinds of behavior cause sexual harassment are,

- Verbal
  Verbal harassment can be commenting about someone's clothing, making body gender-based jocks, spreading rumors about a person's sexual life, threats, using foul and obscene language, Requesting sexual favors, or repeatedly asking a person out.

- Visual
  Visual harassment can include posters, drawings, screen savers, pictures, cartoons, emails, or text of sexual nature.

- Physical
  Physical harassment includes inappropriate touching like kissing, hugging rubbing, etc., and blocking movement, sexual gesturing, leering or staring.

The two categories of sexual harassment are:

##### Quid Pro Quo

Quid pro quo sexual harassment occurs usually when a manager asks for sexual favors from a worker in return for some type of job benefit like a raise, better hours, promotion, etc.

This also occurs when a manager or other authority says he or she will not fire or reprimand an employee in exchange for some type of sexual favor.

##### Hostile Work Environment

This occurs when employee behavior interferes with the work performance of another and creates an offensive workplace.

This usually occurs when someone makes a repeated sexual comment and makes the employee feel uncomfortable. This will affect the work habits of that employee and it may lead to taking decisions like quitting the job.

## Question 2:

#### What would you do in case you face or witness any incident or repeated incidents of such behaviour?

In case I face or witness any incident or repeated incidents of sexual harassment

- First I will directly talk to the harasser and I will say it clearly that it is offending me and ask that person to stop doing that.
- Even If the person did not stop it I will complain to the supervisor with a written complaint and with some proof to take action against that person.

## Question 3:

#### Explains different scenarios enacted by actors.

There were many examples explained in the video, some scenarios are explained below.

- You got a keep trying :
  In this, they enacted an example of forcing someone out, even after informing clearly about not having interest. This will lead to a hostile work environment.

- The legend :
  In this they enacted an example of people with a different mindset in situations like giving hug, It's not the same for all someone may feel comfortable in giving hug, and some not, but nothing has to be forced this will also lead to a hostile work environment.

- Testing the waters:
  Here they enacted a situation where the supervisor was trying to test her personal interaction this made her feel uncomfortable and it destroys an excellent working relationship.

## Question 5:

#### How to handle cases of harassment?

- First, we have to make the effort to notice how to stop it.
- Need to document if anything we can do with it later.
- Should try to inform the harasser about your discomfort.
- If nothing has stopped after intimating should complain to the concerned team.

## Question 6:

#### How to behave appropriately?

- It is important to behave appropriately to protect ourselves and others from sexual harassment.

- We have to make it clear that the behavior is not acceptable. And use clear and direct language to communicate our boundaries.

- We have to Keep a record of any incidents of harassment, including the date, time, location, what was said or done, and any witnesses who were present.

- We have to report the harassment to a supervisor, HR, or other appropriate authority figures as soon as possible.

- Talk to someone we trust, whether it's a friend, family member, or therapist. It's important to have emotional support in this situation.
